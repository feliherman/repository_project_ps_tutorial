package com.example.flashscoreproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginPage extends Activity {


    EditText email;
    EditText pass;

    Button login;

    User posts;

    List<Team> teams = new ArrayList<Team>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagelogin);

        email = (EditText)findViewById(R.id.editTextTextEmailAddress);
        pass = (EditText)findViewById(R.id.editTextTextPassword);

        login = (Button)findViewById(R.id.button3);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Email este : " + email.getText().toString());
                System.out.println("Parola este : " + pass.getText().toString());

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://eabf747e51db.ngrok.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

            Call<User> call = jsonPlaceHolderApi.getLogin(email.getText().toString(),pass.getText().toString());

            Call<List<Team>> callTeams = jsonPlaceHolderApi.getTeams();

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if(!response.isSuccessful()){
                        System.out.println("Code: " + response.code());
                        return;
                    }


                    posts = response.body();
                    Intent intent = new Intent(LoginPage.this, LogedInUserPage.class);
                    intent.putExtra("UserName",posts.getName());
                    intent.putExtra("UserEmail",posts.getEmail());
                    intent.putExtra("UserTeam",posts.getFavouriteTeam());

                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    System.out.println("Invalid Login! Please try again!");
                    email.setText("");
                    pass.setText("");
                }

            });
            }
        });
    }

}
