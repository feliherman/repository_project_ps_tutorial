package com.example.flashscoreproject;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserPage extends Activity {

    private TextView textViewResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pageuser);


        textViewResult = findViewById(R.id.textView7);
        textViewResult.setMovementMethod(new ScrollingMovementMethod());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://eabf747e51db.ngrok.io")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<User>> call = jsonPlaceHolderApi.getUsers();

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(!response.isSuccessful()){
                    textViewResult.setText("Code: " + response.code());
                    return;
                }

                List<User> posts = response.body();

                for(User post : posts){
                    String aux = "";
                    aux += "Id: " + post.getId() + "\n";
                    aux += "Name: " + post.getName() + "\n";
                    aux += "Email: " + post.getEmail() + "\n";
                    aux += "Favourite Team: "+ post.getFavouriteTeam() + "\n\n\n";
                    textViewResult.append(aux);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }
}
