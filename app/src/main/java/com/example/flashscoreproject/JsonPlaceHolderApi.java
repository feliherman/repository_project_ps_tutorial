package com.example.flashscoreproject;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface JsonPlaceHolderApi {

    @GET("/demo/all")
    Call<List<User>> getUsers();

    @GET("/demo/allTeamsSorted")
    Call<List<Team>> getTeams();

    @GET("/demo/login")
    Call<User> getLogin(@Query("email") String email,@Query("password") String password);

    @GET("/demo/firstTeams")
    Call<List<Team>> getFirstTeams();

    @POST("demo/add")
    Call<String> addNewUser(@Query("name") String name,@Query("email") String email,@Query("password") String password,@Query("favouriteTeam") String favouriteTeam);
}
