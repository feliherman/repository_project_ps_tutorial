package com.example.flashscoreproject;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class DetailsPage extends Activity {

    private TextView text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagedetails);

        text = findViewById(R.id.textView9);

        String userName = getIntent().getStringExtra("UserName");
        String userEmail = getIntent().getStringExtra("UserEmail");
        String userTeam = getIntent().getStringExtra("UserTeam");
        String userFlag = getIntent().getStringExtra("Flag");
        text.append("Name   :   " + userName + "\n\n" + "Email   :   " + userEmail + "\n\n" + "Favourite Team : " + userTeam + "\n\n");
        if(userFlag.equals("Da"))
            text.append("\n\n" + "Bravo, echipa ta este in top 3!");
        else
            text.append("\n\n" + "Echipa ta nu este in top!");

    }
}
