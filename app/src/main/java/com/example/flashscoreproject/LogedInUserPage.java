package com.example.flashscoreproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LogedInUserPage extends Activity {

    private TextView text;
    private TextView textViewResult;

    private Button logOut;
    private Button details;

    private int pres = 0;

    ArrayList<Team> teams = new ArrayList<Team>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagelogedin);

        text = (TextView)findViewById(R.id.textView10);

        textViewResult = findViewById(R.id.textView11);
        textViewResult.setMovementMethod(new ScrollingMovementMethod());

        logOut = findViewById(R.id.button5);
        details = findViewById(R.id.button7);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://eabf747e51db.ngrok.io")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<Team>> call = jsonPlaceHolderApi.getTeams();

        String userName = getIntent().getStringExtra("UserName");
        String userEmail = getIntent().getStringExtra("UserEmail");
        String userTeam = getIntent().getStringExtra("UserTeam");

        text.append(userName);

        call.enqueue(new Callback<List<Team>>() {
                         @Override
                         public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                             if (!response.isSuccessful()) {
                                 textViewResult.setText("Code: " + response.code());
                                 return;
                             }

                             List<Team> posts = response.body();

                             String upTable = "\n\n" + "Team" + "\t\t\t\t\t" + "GamesPlayed" + "\t\t\t" + "Points" + "\t\t\t" + "Goals" + "\t\t\t" + "TakenGoals" + "\n\n\n";

                             textViewResult.append(upTable);
                             for (Team post : posts) {
                                 String aux = "";
                                 aux += post.getName();
                                 for (int i = 0; i < (25 - post.getName().length()); i++)
                                     aux += " ";
                                 aux += post.getGamesPlayed() + "                    ";
                                 aux += post.getPoints() + "                ";
                                 aux += post.getGoalsScored() + "               ";
                                 aux += post.getGoalsTaken() + " \n\n";
                                 textViewResult.append(aux);
                                 teams.add(post);
                                 }
                             }

                         @Override
                         public void onFailure(Call<List<Team>> call, Throwable t) {
                             textViewResult.setText(t.getMessage());

                         }
                     });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userFlag = "";
                int loc = 3;
                for(Team post : teams) {
                    if (loc > 0)
                        if (post.getName().equals(userTeam))
                            pres = 1;
                    loc--;
                }
                if(pres == 1)
                    userFlag+="Da";
                else
                    userFlag+="Nu";
                String finalUserFlag = userFlag;
                Intent intent = new Intent(LogedInUserPage.this, DetailsPage.class);
                intent.putExtra("UserName",userName);
                intent.putExtra("UserEmail",userEmail);
                intent.putExtra("UserTeam",userTeam);
                intent.putExtra("Flag", finalUserFlag);
                startActivity(intent);
            }
        });
        System.out.println("Avem userul " + userName + userEmail + userTeam);
    }
}
