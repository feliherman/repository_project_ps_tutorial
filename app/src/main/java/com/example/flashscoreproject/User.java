package com.example.flashscoreproject;

import com.google.gson.annotations.SerializedName;

public class User {
    private int userId;
    private int id;
    private String name;
    private String email;
    private String favouriteTeam;
    private String password;

    @SerializedName("Text")
    private String text;


    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getFavouriteTeam() {
        return favouriteTeam;
    }

    public String getPassword() {
        return password;
    }

    public String getText() {
        return text;
    }
}
